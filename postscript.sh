#!/bin/bash
. trap_print
mkdir -pv ${target}/builds/{.npm-packages/{bin,share/man,lib/node_modules},.gems}
echo "prefix = /builds/.npm-packages" > ${target}/builds/.npmrc
chmod -R 770 ${target}/builds/.npm-packages
chmod -R 770 ${target}/builds/.gems
unlink ${target}/etc/alternatives/ld
ln -sf /usr/bin/ld.gold ${target}/etc/alternatives/ld