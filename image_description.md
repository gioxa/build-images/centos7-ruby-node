# ${ODAGRUN_IMAGE_REFNAME}


[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

## Description

CentOS7 docker image with:
- ruby 2.51
- bundle
- nodejs V10.+
- npm
- yarn

with locale support and i18n.

**Weekly Updated** with gitlab trigger schedule

## purpose

Image for building ruby web apps

## preinstalled ruby packages

```bash
# GEMS:

$GEM_LIST
```
## application usage:

with `GEM_HOME` set to `/builds/.gems`, packages with bundler install in ci builds can be cached as `/builds/.gems` when using:

```bash
bundle install --system --no-deployment  --no-prune --retry=2
```

## Image Config

```yaml
$DOCKER_CONFIG_YML
```
---

*Build with [odagrun](https://www.odagrun.com)*
