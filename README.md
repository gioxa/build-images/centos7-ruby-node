[![](https://images.microbadger.com/badges/image/gioxa/centos7-ruby-node.svg)](https://microbadger.com/images/gioxa/centos7-ruby-node "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/gioxa/centos7-ruby-node.svg)](https://microbadger.com/images/gioxa/centos7-ruby-node "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/license/gioxa/centos7-ruby-node.svg)](https://microbadger.com/images/gioxa/centos7-ruby-node "Get your own license badge on microbadger.com")

# centOS7-ruby-node

## purpose

Build image for static web development

CentOS7 docker image with:
- ruby 2.51
- bundle
- nodejs v10 +
- npm
- yarn

## usage

```yaml
image: gioxa/ruby-node-centos:latest
```
---

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*